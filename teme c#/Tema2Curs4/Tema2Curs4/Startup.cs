﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Tema2Curs4.Models;

namespace Tema2Curs4
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddTransient<IProductRepository, EFProductRepository>();

            services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(_configuration["ConnectionStrings:DefaultConnection"]));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //app.UseMvc(configureRoutes);
            app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();

            app.UseMvcWithDefaultRoute();
        }


        //private void configureRoutes(IRouteBuilder routes)
        //{
        //    routes.MapRoute(null, "{controller=Product}/{action=GetAll}");
        //    //routes.MapRoute(null, "{controller=Product}/{action=GetById}");

        //}
    }
}
