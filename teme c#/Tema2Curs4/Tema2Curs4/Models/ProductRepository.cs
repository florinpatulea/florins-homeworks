﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tema2Curs4.Models
{
    public interface IProductRepository
    {
        IEnumerable<Product> Products { get; }
    }
    public class ProductRepository : IProductRepository
    {
        readonly List<Product> _products = new List<Product>
        {
            new Product
            {
                Id = 1,
                Name = "Carne",
                Description= "tocata"
            },
             new Product
            {
                Id = 2,
                Name = "Branza",
                Description= "vaci"
            }
        };

        public IEnumerable<Product> Products => _products;
    }
}
