﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tema2Curs4.Models;

namespace Tema2Curs4.Controllers
{
    public class ProductController : Controller
    {
        readonly IProductRepository _repo;

        public ProductController(IProductRepository repo)
        {
            _repo = repo;
        }
        //public IActionResult Index()
        //{
        //    var products = _repo.Products;
        //    return View(products) ;
        //}

        public IActionResult GetAll()
        {
            var all = _repo.Products;
            return View(all);
        }

        //public IActionResult GetById(int id)
        //{
        //    foreach(var p in _repo)
        //    {
        //        if (p.Id = id)
        //        {
        //            return View(_repo);
        //        }
        //    }
            
        //}
    }
}