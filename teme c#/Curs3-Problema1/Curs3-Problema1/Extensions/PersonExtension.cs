﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Curs3_Problema1.Extensions
{
    public static class PersonExtension
    {
        public static void DisplayAge(this Person p)
        {
            Console.WriteLine(p.Age);

        }
    }
}
