﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Curs3_Problema1.Extensions;

namespace Curs3_Problema1
{
    public  class Person //sealed
    {
        public const string Value = "123";
        readonly string _name;
        readonly int _age;

        public Person(string name, int age)
        {
            _name = name;
            _age = age;
        }
        
        public void DisplayName()
        {
            
            Console.WriteLine(_name);
        }

        public int Age => _age;
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class Employee : Person
    {
        public Employee(string name, int age) : base(name, age)
        {

        }
    }
    class Program
    {

        static void AnonymoustTest()
        {
            var persons = new List<Person>
            {
                new Person("1",10)
                {
                    FirstName = "a",
                    LastName= "b"
                },
                new Person("2",12)
                {
                    FirstName = "c",
                    LastName= "d"
                },
                new Person("3",14)
                {
                    FirstName = "e",
                    LastName= "f"
                }
            };

            var result = persons.Select(x => new { x.FirstName, x.LastName });
            foreach(var value in result)
            {
                Console.WriteLine($"{value.FirstName} {value.LastName}");
            }

        }
        static int Add(int x, int y) => x + y;
        static void Main(string[] args)
        {
            //Person p = new Person("John", 10);
            //p.DisplayName();
            //p.DisplayAge();
            //Employee e = new Employee("Tom", 12);
            //e.DisplayName();
            //e.DisplayAge();

            Console.WriteLine(Add(10, 20));

            //delegati def in .net
            //Func - returneaza tip de date
            //    Action- nu returneaza nimic

            Action<int, int> add = (x, y) => Console.WriteLine(x + y);
            add(15, 25);

            Func<int, int, int> addFunc = (x, y) => x + y;
            Console.WriteLine(addFunc(20, 30));

            AnonymoustTest();



        }
    }
}
