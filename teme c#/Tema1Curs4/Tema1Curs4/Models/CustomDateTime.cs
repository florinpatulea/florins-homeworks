﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tema1Curs4.Models
{
    public class CustomDateTime
    {

        public int Data { get; set; }

        public int Day { get; set; }

        public int Hour { get; set; }
    }
}
