﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tema1Curs4.Models
{
    public class DateRepository
    {
        readonly List<CustomDateTime> _date = new List<CustomDateTime>
        {
            new CustomDateTime
            {
                Data = 12,
                Day = 15,
                Hour = 10
            },
             new CustomDateTime
            {
                Data = 13,
                Day = 11,
                Hour = 12
            }

        };

        public List<CustomDateTime> Dates => _date;
    }
}
