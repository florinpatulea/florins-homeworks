﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tema1Curs4.Models;

namespace Tema1Curs4.Controllers
{
    public class DateController : Controller
    {
        readonly DateRepository _repo;

        public DateController (DateRepository repo)
        {
            _repo = repo;
        }
        public IActionResult Index()
        {
            var dates = _repo.Dates;
            return View(dates);
        }
    }
}