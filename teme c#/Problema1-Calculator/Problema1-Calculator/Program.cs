﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema1_Calculator
{
    class Program
    {
        

        public static float Adunare(float a, float b)
        {
            return a + b;
        }

        public static float Scadere(float a, float b)
        {
            return a - b;
        }

        public static float Inmultire(float a, float b)
        {
            return a * b;
        }

        public static float Impartire(float a, float b)
        {
            return a / b;
        }
        static void Main(string[] args)
        {
            bool d = true;


           
            while (d == true)
            {
                Console.WriteLine("Alegeti operatia dorita:");
                Console.WriteLine("1.Adunare");
                Console.WriteLine("2.Scadere");
                Console.WriteLine("3.Inmultire");
                Console.WriteLine("4.Impartire");
                Console.WriteLine("5.Finalizare program");



                 string a = Console.ReadLine();
                 int a1 = Convert.ToInt32(a);


                switch (a1)
                {
                    case 1:
                        Console.WriteLine("Adunare");
                        {
                            Console.WriteLine("Introduceti numerele:");
                            string b = Console.ReadLine();
                            int Adun1 = Convert.ToInt32(b);
                            string c = Console.ReadLine();
                            int Adun2 = Convert.ToInt32(c);
                            float Adun = Adunare(Adun1, Adun2);
                            Console.WriteLine(Adun);
                            Console.WriteLine(" ======================");
                            


                            break;
                        }
                        
                    case 2:
                        Console.WriteLine("Scadere");
                        {
                            Console.WriteLine("Introduceti numerele:");
                            string b = Console.ReadLine();
                            int Scad1 = Convert.ToInt32(b);
                            string c = Console.ReadLine();
                            int Scad2 = Convert.ToInt32(c);
                            float Scad = Scadere(Scad1, Scad2);
                            Console.WriteLine(Scad);
                            Console.WriteLine(" ======================");
                            break;
                        }
                        

                    case 3:
                        Console.WriteLine("Inmultire");
                        {
                            Console.WriteLine("Introduceti numerele:");
                            string b = Console.ReadLine();
                            int Inmu1 = Convert.ToInt32(b);
                            string c = Console.ReadLine();
                            int Inmu2 = Convert.ToInt32(c);
                            float Inmu = Inmultire(Inmu1, Inmu2);
                            Console.WriteLine(Inmu);
                            Console.WriteLine(" ======================");
                            break;
                        }
                        

                    case 4:
                        Console.WriteLine("Impartire");
                        {
                            Console.WriteLine("Introduceti numerele:");
                            string b = Console.ReadLine();
                            int Imp1 = Convert.ToInt32(b);
                            string c = Console.ReadLine();
                            int Imp2 = Convert.ToInt32(c);
                            float Imp = Impartire(Imp1, Imp2);
                            Console.WriteLine(Imp);
                            Console.WriteLine(" ======================");
                            break;
                        }
                        
                    default:
                        {
                            Console.WriteLine("Nu ati selectat operatie. Iesire din program");
                            d = false; ;
                            Console.WriteLine(" ======================");
                            break;
                        }

                }
            }
        }
    }
}
