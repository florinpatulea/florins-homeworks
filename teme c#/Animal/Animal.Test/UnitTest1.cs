﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Animal;

namespace Animal.Test
{
    [TestClass]
    public class UnitTest1
    {

        Animals a1 = new Animals()
        {
            Name = "Tiger",
            Description = "Red",
            Id = 1

        };


        Animals a2 = new Animals()
        {
            Name = "Elephant",
            Description = "Green",
            Id = 2

        };
        [TestMethod]
        public void TestMethod1()
        {
            AnimalRepository repo = new AnimalRepository();
            repo.Add(a1);
            repo.Add(a2);
            Animals first = repo.GetById(1);
            Animals second = repo.GetById(2);


            Assert.AreEqual(a1.Name, first.Name);
            Assert.AreEqual(a1.Description, first.Description);
            Assert.AreEqual(a1.Id, first.Id);



        }
    }
}
