package ro.cerner.practica.course.nestedClasses;

public class OuterClass {
	
	private String name;
	private static boolean state;
	
	public class MemberInnerClass{
		
		public void method() {
			name = "";
			state = true;
		}
		
	}
	
	public void myMethod() {
		
		class LocalInnerClass{
			public void method() {
				name ="";
				state = true;
			}
		}
		LocalInnerClass lic = new LocalInnerClass();
	}
	
	public static void main(String[] args) {
		
		
		MyInterface mi = new MyInterface() {
			//uniquely implementation of MyInterface methods declaration
		};
		
		MyClass mc = new MyClass(){
			//override whatever we are interested in
		};
	}
	
	static class StaticNestedClass {
		
	}

}
