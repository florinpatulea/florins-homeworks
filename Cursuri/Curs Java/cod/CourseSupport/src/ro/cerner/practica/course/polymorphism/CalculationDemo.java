package ro.cerner.practica.course.polymorphism;

class Calculation {

    int add(int a, int b)
    {
         return a + b;
    }
    
    int  add(int a, int b, int c)  
    {
         return a + b + c;
    }
    
    double add(double a, double b)
    {
         return a + b;
    }
}

public class CalculationDemo
{
   public static void main(String args[])
   {
	   Calculation obj = new Calculation();
	   
       System.out.println(obj.add(10, 20));
       System.out.println(obj.add(10, 20, 30));
       System.out.println(obj.add(10d, 20));
   }
}
