package ro.cerner.practica.course.primitives.pack2;

public class DemoAnimal {
	
	public static void changeAnimal (Animal animal){
		animal.setSound("Sound1");
		
		animal = new Cat();
		
		animal.setSound("Sound2");
	}
	
	public static void main(String[] args) {
		
		Animal animal = new Animal();
		System.out.println(animal.getSound());
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		changeAnimal(animal);
		System.out.println(animal.getSound());
	}

}
