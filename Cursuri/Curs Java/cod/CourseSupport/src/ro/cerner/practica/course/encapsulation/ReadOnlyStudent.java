package ro.cerner.practica.course.encapsulation;

public class ReadOnlyStudent {
	
	//private data member  
	private String college="AKG";  
	
	//getter method for college  
	public String getCollege(){  
		return college;  
	}  
}  

