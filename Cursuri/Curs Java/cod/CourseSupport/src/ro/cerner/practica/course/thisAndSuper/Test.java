package ro.cerner.practica.course.thisAndSuper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Test {
	
	public static void main(String[] args) {
		
		List<Person> persons = new ArrayList<>();
		persons.add(new Person(44));
		persons.add(new Person(3));
		persons.add(new Person(13));
		
		
		Comparator<Person> byAgeLambda = ( o1,  o2) ->  o1.age - o2.age;
		Comparator byAge = new Comparator<Person>(){
			@Override
			public int compare(Person o1, Person o2) {
				return o1.age - o2.age;
			}
		};
		
		persons.sort(( o1,  o2) ->  o1.age - o2.age);
		
		//System.err.println();
		persons.forEach(System.out::println);
		
		
	}

}
