package ro.cerner.practica.course.primitives.pack1;

public class DemoAnimal {
	
	public static void changeAnimal (Animal animal){
		animal = new Cat();
	}
	
	public static void main(String[] args) {
		
		Animal animal = new Animal();
		System.out.println(animal.getSound());
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		changeAnimal(animal);
		System.out.println(animal.getSound());
	}

}
