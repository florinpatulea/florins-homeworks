package ro.cerner.practica.course.modifiers;

public abstract class Shape {
	
	private String name;
	
	protected static int nrOfShapes;
	
	int  shapeIndex;
	
	public abstract double calculateArea();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
