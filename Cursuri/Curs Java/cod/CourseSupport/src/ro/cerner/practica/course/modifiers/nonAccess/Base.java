package ro.cerner.practica.course.modifiers.nonAccess;

public class Base {
	
	private void foo() {
		System.out.println("Foo");
	} 
	
	public static void main(String[] args) {
		Base b = new Base();
		b.foo();
	}

}
