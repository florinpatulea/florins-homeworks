package ro.cerner.practica.course.primitives.pack2;
public class Cat extends Animal{
	
	private String sound = "Cat";

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}
	
}
