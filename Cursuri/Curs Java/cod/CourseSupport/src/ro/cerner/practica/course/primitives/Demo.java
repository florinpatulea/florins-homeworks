package ro.cerner.practica.course.primitives;

public class Demo {
	
	public static void increment ( int[] a, int n){
		a[0]++;
		a[1]++;
		
		n++;
	}
	
	public static void main(String[] args) {
		int[] a = {1, 2, 3};
		
		int n = 10;
		
		System.out.println("a[" + a[0] + ", " + a[1] + ", " + a[2] + "]");
		System.out.println("n = " + n);
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		increment(a, n);
		System.out.println("a[" + a[0] + ", " + a[1] + ", " + a[2] + "]");
		System.out.println("n = " + n);
		

	}
	


}
