package ro.cerner.practica.course.encapsulation;

public class TestStudent {
	
	public static void main(String[] args) {
		
		Student s1 = new Student();
		s1.setName("Student 1");
		System.out.println(s1.getName());
		
		ReadOnlyStudent s2 = new ReadOnlyStudent();
		System.out.println(s2.getCollege());

		WriteOnlyStudent  s3 = new WriteOnlyStudent();
		s3.setCollege("Transilvania");
	}

}
