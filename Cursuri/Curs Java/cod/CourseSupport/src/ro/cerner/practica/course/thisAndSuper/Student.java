package ro.cerner.practica.course.thisAndSuper;

public class Student extends Person{
	
	String specialization;
	
	public Student(String specialization, int age) {
		super(age);
		//this();
		
		//specialization = specialization;
		//this.specialization = specialization;
		//super.specialization = specialization;
		
	    //age = age;
		//this.age = age;
		//super.age = age;
		
		nrOfPeople ++;
	}
	
	public int getAge() {
		//return super.age;
		// return this.age;
		// return age;
		return 0;
	}
	
	public int getNrOfPeople() {
		// return super.nrOfPeople;
		// return this.nrOfPeople;
		// return Person.nrOfPeople;
		// return nrOfPeople;
		//Person p = super();
		return 0;
	}
	
	public static void main(String[] args) {
		nrOfPeople = 3;
	}
	
}
