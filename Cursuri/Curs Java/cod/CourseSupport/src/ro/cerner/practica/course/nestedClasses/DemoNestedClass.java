package ro.cerner.practica.course.nestedClasses;

public class DemoNestedClass {
	
	public static void main(String[] args) {
		
		OuterClass oc = new OuterClass();
		System.out.println("OuterClass = " + oc);
		
		OuterClass.StaticNestedClass snc = new OuterClass.StaticNestedClass();
		System.out.println("StaticNestedClass = " + snc);
		
		OuterClass.MemberInnerClass mic = oc.new MemberInnerClass();
		System.out.println("MemberInnerClass = " + mic);
	}

}
