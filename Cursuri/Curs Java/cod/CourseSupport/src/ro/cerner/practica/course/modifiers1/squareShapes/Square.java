package ro.cerner.practica.course.modifiers1.squareShapes;

import ro.cerner.practica.course.modifiers.Shape;

public class Square extends Shape{
	
	private double side;
	
	public Square(double side) {
		setName("Square");
		this.side = side;
		Shape.nrOfShapes ++;
	}

	@Override
	public double calculateArea() {
		return side * side;
	}

}
