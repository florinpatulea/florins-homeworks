package ro.cerner.practica.course.modifiers.roundShapes;

import ro.cerner.practica.course.modifiers.Shape;

public class Circle extends Shape {
	
	private double radius;
	
	public Circle(double radius) {
		this.radius = radius;
		setName("Circle");
		Shape.nrOfShapes ++;
	//	shapeIndex = 1;
	}

	@Override
	public double calculateArea() {
		return radius * radius * Math.PI;
	}
}
