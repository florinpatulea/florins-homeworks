package ro.cerner.practica.course.overloading;

public class Demo {
	
	public Demo() {
		this("");
	}
	
	Demo(String s) {
		
	}
	
	// Different number of parameters
	public int add(int x, int y) {
		return x + y;
	}
	
	public int add(int x, int y, int z) {
		return x + y + z;
	}
	
	

}
