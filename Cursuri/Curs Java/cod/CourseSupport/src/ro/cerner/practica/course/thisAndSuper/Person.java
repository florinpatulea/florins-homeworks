package ro.cerner.practica.course.thisAndSuper;

public class Person {
	@Override
	public String toString() {
		return "Person [age=" + age + "]";
	}

	int age;
	
	static int nrOfPeople = 0;
	
	public Person(int age) {
		this.age = age;
	}

}
