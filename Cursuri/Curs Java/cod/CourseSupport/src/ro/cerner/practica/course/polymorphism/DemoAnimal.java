package ro.cerner.practica.course.polymorphism;

import java.util.ArrayList;
import java.util.List;

class Animal {    

	public void getBread(){
	      System.out.println("This is an Animal!");
	   }
	
	@Override
	public String toString() {
		return "Animal";
	}
}

class Dog extends Animal {

	public void getBread() {
      System.out.println("This is a Dog!");
   }
	
	public long getNrPurici(){
		return 1L;
	}
	
	@Override
	public String toString() {
		return "Dog";
	}
}


public class DemoAnimal {

 public static void main(String args[]) {
      Animal a = new Animal(); // Animal reference and object
      Animal b = new Dog(); // Animal reference but Dog object
      
      a.getBread();
      b.getBread();
      
      ((Dog)b).getNrPurici();
	      
      List<Animal> animals = new ArrayList<>();
      animals.add(a);
      animals.add(b);
      animals.forEach(System.out::println);
	  }

}
