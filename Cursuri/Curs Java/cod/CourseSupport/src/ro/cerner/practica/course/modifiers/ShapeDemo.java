package ro.cerner.practica.course.modifiers;

import ro.cerner.practica.course.modifiers.roundShapes.Circle;
import ro.cerner.practica.course.modifiers1.squareShapes.Square;

public class ShapeDemo {
	
	public static void main(String[] args) {
		
		Shape s = new Circle (1);
		Circle c = new Circle (2);
		Square sq = new Square(4);
		
		//access private members
	    //c.radius = 4;
		
		//access default members
		System.out.println(s.shapeIndex); //same package
		//System.out.println(c.shapeIndex); //different package
		
		//access protected members
		System.out.println(Shape.nrOfShapes);
		
		//
	}

}
