package ro.cerner.practica.course.primitives.pack2;
public class Animal {
	
	private String sound = "Animal";

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}
}

