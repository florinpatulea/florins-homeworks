package ro.cerner.practica.course.literals;

public class LiteralsDemo {
	
	public static void main(String[] args) {
		
		boolean flag = true; //boolean literal
		
		
		//Numeric literal
		
		//Decimal literal (Base 10) : In this form the allowed digits are 0-9.
		int x = 9870;  
		System.out.println(x);
		
		//Octal literals (Base 8) : In this form the allowed digits are 0-7.
		// The octal number should be prefix with 0.
		int y = 0146; 
		System.out.println(y);
		
		//Hexa-decimal literals (Base 16) : In this form the allowed digits are 0-9 and characters are a-f. We can use both uppercase and lowercase characters. As we know that java is a case-sensitive programming language but here java is not case-sensitive.
		// The hexa-decimal number should be prefix with 0X or 0x.
		int z = 0X123Face; 
		System.out.println(z);
		
		//Binary literals : From 1.7 onward we can specify literals value even in binary form also, allowed digits are 0 and 1. Literals value should be prefixed with 0b or 0B.
		int w = 0b1111;
		System.out.println(w);
		
		//floating point literal
		float f = 2.3f; 
		System.out.println(f);
		
		double decimalNumber = 5.0;
		System.out.println(decimalNumber);
		
		decimalNumber = 89d;
		System.out.println(decimalNumber);
		
		decimalNumber = 0.5;
		System.out.println(decimalNumber);
		
		decimalNumber = 10f;
		System.out.println(decimalNumber);
		
		decimalNumber = 3.14159e0;
		System.out.println(decimalNumber);
		
		decimalNumber = 2.718281828459045D;
		System.out.println(decimalNumber);
		
		decimalNumber = 1.0e-6D;
		System.out.println(decimalNumber);
		
		//Char literal
		char ch = 'a';
		System.out.println(ch);
		
		char c = '\u0061'; // Unicode representation 
		System.out.println(c);
		
		//String literal
		String s = "Hello";
		System.out.println(s);
		 
		
	}

}
