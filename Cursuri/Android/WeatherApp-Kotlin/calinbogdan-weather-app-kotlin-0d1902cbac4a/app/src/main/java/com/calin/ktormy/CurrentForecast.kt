package com.calin.ktormy

class CurrentForecast(val temperature: Double,
                      val humidity: Double,
                      val precipProbability: Double)