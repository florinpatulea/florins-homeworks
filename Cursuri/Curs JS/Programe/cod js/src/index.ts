import express, { response } from "express"; // express - creeaza server
import bodyParser from "body-parser";
import * as mockedData from "../resources/MockedListData.json";
import { request } from "http";
const app = express();
const port = 8080;



app.use (bodyParser.json()); 
app.use(bodyParser.urlencoded({extended: false}));
app.get("/", (request, response)=> {

    response.send("Hello world");
});

app.get ("/lists",(req,res)=> {
    res.send(mockedData.lists);
});

app.get("/list/:id", (req,res) => {
    let idFromRequest = req.params.id;
    let requestList = mockedData.lists.filter(el => el.id === idFromRequest);
    if (requestList) {
        res.send(requestList); 
    } else {
        res.send("No list found");
    }
});

app.get("/greetings",(request,response) => {
    response.send(`Welcome Florin!`);
});

app.get("/greetings/:name", (request,response) => {
    response.send(`Welcome ${request.params.name}!`);
});

app.get("/list/:id/:id1", (request,response) => {
    let idFromRequest = request.params.id;
    let id1FromRequest  = request.params.id1;
    let requestList = mockedData.lists.filter(el => el.id === idFromRequest);
 //   let requestList1 = mockedData.lists.filter(requestList => requestList.id === id1FromRequest);
  // let requestList1 = requestList.filter(el1 => el1.id === id1FromRequest);
 // let requestList1 = requestList.filter(requestList => requestList.listElements.filter(el => el.id === id1FromRequest));
    //! - not null - not working
    let requestList1 = requestList[0].listElements.filter(el1 => el1.id ===id1FromRequest);
    
    response.send(requestList1);
    
    
    
});

app.post("/adaugare", (request,response) => {
        let a = request.body.id;
        let b = request.body.elementID
   console.log(request.body.listID);
        let c = request.body.title;
        let d = request.body.body;
        // let id="4";
        // let elementID = "1";
        // let title = "dasd";
        // let body = "dasdd";
        
        // console.log(a);
        // console.log(b);
        // console.log(c);
        // console.log(d);

       
       // let requestList = mockedData.lists.push(a,b,c,d);
      //  response.send(requestList);
      
        let requestList = mockedData.lists.concat(a,b,c,d);
          // response.send(mockedData.lists.concat(id,elementID,title,body));
            response.send(requestList);
         //  response.send(` + "id:" ${request.body.listID}  "listElements:"  "id:" + ${request.body.elementID} "title:"+ ${request.body.title}  "body:" + ${request.body.body}`);

    
});

app.delete("/liststergere/:id/", (request,response) => {
    let idFromRequest = request.params.id;
// filtrare listElements si afisare
    let requestList = mockedData.lists.splice(2, 1);

    response.send(mockedData.lists);
   
   
    
     
    
    
    


});

app.post("/toConsole", (req,res)=>{
console.log(req.body.myMessage);

});

app.listen(port, () => {
console.log(`Server started at http://localhost:${port}!`)

});

