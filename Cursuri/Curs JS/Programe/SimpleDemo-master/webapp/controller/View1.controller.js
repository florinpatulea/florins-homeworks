sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("com.cerner.interns.SAPUI5_Demo.controller.View1", {
		onInit: function () {
			var model = new sap.ui.model.json.JSONModel();
			model.loadData("./api/data.json");
			this.getView().setModel(model, "patients");
		},
		onClick : function(event){
			alert("Hello");
			debugger;
		}
	});
});