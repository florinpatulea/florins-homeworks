import passport from "passport";
import passportLocal from "passport-local";
import User from "../models/User";

const LocalStrategy = passportLocal.Strategy;

passport.serializeUser<any, any>((user, done) => {
    done(undefined, user.id);
});

passport.deserializeUser(async (id, done) => {
    let user = User.findById(id);
    done(undefined, user);
});

passport.use(
    new LocalStrategy(
        { usernameField: "email", passwordField: "password" },
        async (email, password, done) => {
            try {
                let user = await User.findOne({ email: email });
                if (user) {
                    if (user.validPassword(password)) {
                        return done(null, user);
                    } else {
                        return done(null, false);
                    }
                } else {
                    return done(null, false);
                }
            } catch {
                return done(null);
            }
        }
    )
);
