import express from "express";
import DriverManager from "../services/DriverManager";

export default (app: express.Application, passport?: any) => {
    app.get("/", (request: express.Request, response: express.Response) => {
        response.send("Hello world");
    });

    app.get(
        "/lists",
        (
            req: express.Request,
            res: express.Response,
            next: express.NextFunction
        ) => {
            if (req.isAuthenticated()) {
                next();
            } else {
                res.redirect("/");
            }
        },
        async (req: express.Request, res: express.Response) => {
            try {
                let lists = await DriverManager.Instance.getAllToDoLists();
                if (lists.length > 0) {
                    res.send(lists);
                } else {
                    res.send("No list found!");
                }
            } catch {
                res.send("Error!");
            }
        }
    );

    app.get(
        "/list/:name",
        async (req: express.Request, res: express.Response) => {
            let nameFromRequest = req.params.name;

            try {
                let requestList = await DriverManager.Instance.getListByName(
                    nameFromRequest
                );
                res.send(requestList);
            } catch {
                res.send("Error!");
            }
        }
    );

    app.post(
        "/addList",
        async (req: express.Request, res: express.Response) => {
            try {
                await DriverManager.Instance.addNewList(req.body.name);
                res.send("Success!");
            } catch {
                res.send("Error!");
            }
        }
    );

    app.post(
        "/addListElement",
        async (req: express.Request, res: express.Response) => {
            try {
                await DriverManager.Instance.addNewElementForList(
                    req.body.listName,
                    req.body.taskName,
                    req.body.description
                );

                res.send("Success!");
            } catch {
                res.send("Error!");
            }
        }
    );

    app.post(
        "/login",
        passport.authenticate("local", {
            failureRedirect: "/"
        }),
        (req: express.Request, res: express.Response) => {
            res.send({
                status: "Success",
                username: req.user.username,
                email: req.user.email
            });
        }
    );
};
