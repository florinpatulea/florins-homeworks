import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import List from 'src/app/models/List';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private static readonly BASE_URL = "http://localhost:8080";

  private httpClient: HttpClient;
  constructor(httpClient: HttpClient) { 
    this.httpClient = httpClient;
  }

  getLists(): Promise<List[]> {
    return this.httpClient.get<List[]>(`${TodoService.BASE_URL}/lists`)
              .toPromise();
  }

}
