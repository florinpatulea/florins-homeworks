# Javascript Course

## Access diferent folders to see the code for each course

## QnA general session:

### What were the commands that you guys ran during the live coding session and what do they do?
General: the commands contain unix style flags `-g` or `--global` and `-s` or `--save` for example.

- `npm install -g typescript` - Installing typscript module globally, this includes the compiler.
- `npm install -g @types/node` - Installing the typescript version of the node module.
- `npm install -s express` - Installing the express module, this is the modules that allows us to create the server, the save flag indicates that this module must be saved as a dependency of the current project.
- `npm install -s @types/express` - Installing the typescript version of the express module dependency.
- `npm install -s body-parser` - Installing the body parser module that helps express parse the request/response body automatically into a JSON format.
- `npm install -s mongoose` - Installing the mongoose module, this is the module that makes our life easier when dealing with MongoDB operations.
- `npm install -s @types/mongoose` - Installing the typescript version of the mongoose module dependency.

### Angular related commands
- `npm install -g @angular/cli` - Installing the angular command line tool, this enables us to use the `ng` commands.
- `ng new <Project name>` - Scaffolds a new angular project with the specified name.
- `ng generate` - Generates and/or modifies files based on a schematic.
- `ng generate component <name>` - Scaffolds an empty new component for the currently open angular project
- `ng generate service <name>` - Scaffolds a new service for the currently open angular project
- `ng serve --open` - Runs the angular application and restarts it each time it detects a file change being saved. The optional flag `--open` opens the application in a new tab inside the default browser



