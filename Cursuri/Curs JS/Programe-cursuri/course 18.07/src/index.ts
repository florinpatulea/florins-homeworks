import express from "express";
import bodyParser from "body-parser";
import * as mockedData from "../resources/MockedListData.json";

// testcommit

const app = express();
const port = 8080;

// parse application/x-www-form-urlencoded from body
app.use(bodyParser.json());

//parse application.json from body
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (request, response) => {
	response.send("Hello world");
});

app.get("/greetings", (req, res) => {
	res.send(`Welcome John Doe!`);
});

app.get("/greetings/:name", (req, res) => {
	res.send(`Welcome ${req.params.name}!`);
});

app.get("/lists", (req, res) => {
	res.send(mockedData.lists);
});

app.get("/list/:id", (req, res) => {
	let idFromRequest = req.params.id;
	let requestList = mockedData.lists.find(el => el.id === idFromRequest);
	if (requestList) {
		res.send(requestList);
	} else {
		res.send("No list found.");
	}
});

app.get("/elementFromList/:listId/:elementId", (req, res) => {
	let list = mockedData.lists.find(el => el.id === req.params.listId);
	if (list) {
		let elem = list.listElements.find(el => el.id === req.params.elementId);
		if (elem) {
			res.send({ title: elem.title, body: elem.body });
		} else {
			res.send("No elem found");
		}
	} else {
		res.send("No list found");
	}
});

app.post("/newListElement", (req, res) => {
	let list = mockedData.lists.find(el => el.id === req.body.listId);
	if (list) {
		list.listElements.push({
			id: req.body.elementId,
			title: req.body.title,
			body: req.body.body
		});
		res.send("Success");
	} else {
		res.send("No list found");
	}
});

app.delete("/listElement/:listId/:listElementId", (req, res) => {
	let list = mockedData.lists.find(el => el.id === req.params.listId);
	if (list) {
		list.listElements = list.listElements.filter(el => {
			if (el.id !== req.params.listElementId) {
				return el;
			}
		});
		res.send("Success!");
	} else {
		res.send("No list found");
	}
});

// 1 param "message" read from body
app.post("/toConsole", (req, res) => {
	if (req.body && req.body.message) {
		console.log(req.body.message);
		res.send("Message was posted to server.");
	} else {
		res.send("No message received. Nothing was posted on sever.");
	}
});

app.listen(port, () => {
	console.log(`Server started at http://localhost:${port}!`);
});
