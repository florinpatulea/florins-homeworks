let counter = 0;

const changeHeaderTitle = text => {
    document.getElementById("counter-title")
        .innerHTML = `You clicked me ${text} times.`
};

window.onload = function (event) {
    // changeHeaderTitle(counter);
    document.getElementById("button")
        .addEventListener("click", function (event) {
            counter++;
            changeHeaderTitle(counter);
        });
};