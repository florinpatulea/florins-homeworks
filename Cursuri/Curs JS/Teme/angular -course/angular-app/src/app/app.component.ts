import { Component, OnInit, Renderer } from '@angular/core';
import { TodoService } from './services/todo/todo.service';
import List from './models/list';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  private todoService : TodoService;

    lists: List[];
    selectedRow : Number;
    setClickedRow : Function;


  // public currentList ;
  // public selectList(event: any, list:any){
  //   this.currentList = list.name;
  // }
   
    // setClickedRow : Function;
    // selectedRow : Number;

  constructor(todoService : TodoService) {
 
    this.todoService = todoService;
    this.setClickedRow=function(index){
      this.selectedRow = index;
    };
 
  }
  async ngOnInit() {
    this.lists = await this.todoService.getLists();
    console.log(this.lists);
  }

  public current ;
  public selectList (event :any , list:any){
    this.current = list.name;
  }
}


// export class ListComponent {
  
// }

// export class ClassName {
//   selectedIndex : number =null;

//   setIndex(index:number){
//     this.selectedIndex = index;
//   }
// }


// export class TableComponent {
//   public current;

//   public selectList (event: any, list:any){
//     this.current = list.name;
//   }
// }
