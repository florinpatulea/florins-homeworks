var TodoService_1;
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
let TodoService = TodoService_1 = class TodoService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getLists() {
        return this.httpClient.get(`${TodoService_1.BASE_URL}/lists`)
            .toPromise();
    }
};
TodoService.BASE_URL = "http://localhost:8080";
TodoService = TodoService_1 = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    })
], TodoService);
export { TodoService };
//# sourceMappingURL=todo.service.js.map