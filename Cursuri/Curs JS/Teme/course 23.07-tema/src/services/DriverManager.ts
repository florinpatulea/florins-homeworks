import mongoose from "mongoose";
import ToDoList from "../models/ToDoList";
import ListElement from "../models/ListElement";

export default class DriverManager {
    private MONGO_STRING: string = "mongodb://localhost:27017/ToDoListManager";
    private static _instance: DriverManager;

    private constructor() {}

    public async connect() {
        mongoose.connect(this.MONGO_STRING, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useCreateIndex: true
        });

        let db = mongoose.connection;
        db.once("open", () => {
            console.log("Connected to " + this.MONGO_STRING);
        });
    }

    public static get Instance() {
        if (this._instance) {
            return this._instance;
        } else {
            this._instance = new DriverManager();
            return this._instance;
        }
    }

    public async addNewList(listName: string) {
        let newList = new ToDoList({
            name: listName
        });

        return await newList.save();
    }

    public async getListByName(listName: string) {
        return await ToDoList.findOne({ name: listName }).exec();
    }

    public async getAllToDoLists() {
        return await ToDoList.find({}).exec();
    }

    //public async getSpecificList() {
  //      return await ToDoList.fin
   // }

    public async deleteList(
        listName: string,
        taskName:string
        
    ) {
        let deleteItem = new ListElement({
            title:taskName,
           
        });

        return await ToDoList.findOneAndDelete(
            { name: listName }
            
        ).exec();
    }


    public async deleteElementsList(
        listName: string,
        taskName:string
        
    ) {
      

        return await ToDoList.findOneAndUpdate(
            { name: listName },
            { $pull: { listElements:  {title: taskName } }}
            
            
        ).exec();
    }


    public async addNewElementForList(
        listName: string,
        taskName: string,
        taskDescription: string,
        taskDone: boolean 
    ) {
        let newItem = new ListElement({
            title: taskName,
            body: taskDescription,
            done: taskDone

        });

        return await ToDoList.findOneAndUpdate(
            { name: listName },
            { $push: { listElements: newItem } }
        ).exec();
    }
}
