import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  email: string;
  password: string;

  successShown: boolean = false;
  errorShown: boolean = false;

  constructor(
    private loginService: AuthService, 
    private router: Router) { }

  ngOnInit() { }

  async onClick() {
    let loginWasSuccessful = await this.loginService.login(this.email, this.password);
    if (loginWasSuccessful) {
      this.successShown = true;
      setTimeout(() => {
        this.router.navigate(["/"]);
      }, 1000);
    } else {
      this.errorShown = true;
      setTimeout(() => {
        this.errorShown = false;
      }, 1500);
    }
  }
}
