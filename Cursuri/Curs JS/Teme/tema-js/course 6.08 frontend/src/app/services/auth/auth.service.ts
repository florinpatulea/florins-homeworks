import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ILoginResponse } from './ILoginResponse';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  static readonly API_URL = 'http://localhost:8080';
  static readonly TOKEN_KEY = "MY_PRECIOUS";

  constructor(private httpClient: HttpClient) { }

  set token(token: string) {
    localStorage.setItem(AuthService.TOKEN_KEY, token);
  }

  get token() {
    return localStorage.getItem(AuthService.TOKEN_KEY);
  }

  login(email: string, password: string) {
    return this.httpClient.post(`${AuthService.API_URL}/login`, {
        email: email, 
        password: password
      }).toPromise()
      .then((response: any) => {
        this.token = response.token;
        return true;
      })
      .catch((err) => {
        return false;
      });
    }
}
