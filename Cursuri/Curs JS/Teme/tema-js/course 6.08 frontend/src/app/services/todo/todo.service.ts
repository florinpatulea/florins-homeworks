import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import List from 'src/app/models/List';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private static readonly BASE_URL = "http://localhost:8080";
  constructor(
    private httpClient: HttpClient,
    private loginService: AuthService) { }

  getLists(): Promise<List[]> {
    return this.httpClient.get<List[]>(`${TodoService.BASE_URL}/lists`, {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.loginService.token}`
      })
    }).toPromise();
  }

}
