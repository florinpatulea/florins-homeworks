import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Router } from '@angular/router';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListDetailComponent } from './components/list-detail/list-detail.component';
import { LoginPageComponent } from './components/pages/login-page/login-page.component';

import { routes } from './app.routes';
import { ListsComponent } from './components/pages/lists/lists.component';
import { RegisterPageComponent } from './components/pages/register-page/register-page.component';
import { RecoverPageComponent } from './components/pages/recover-page/recover-page.component';



@NgModule({
  declarations: [
    AppComponent,
    ListDetailComponent,
    LoginPageComponent,
    ListsComponent,
    RegisterPageComponent,
    RecoverPageComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes),
    
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
