import React from 'react';
import logo from './logo.svg';
import './App.css';
import Buttons from './containers/Buttons';
import Sap from './containers/ButtonSAP';
import "@ui5/webcomponents/dist/ShellBar";
import "@ui5/webcomponents/dist/Input.js"; 
import Input from './containers/input';



const x=3; 

function App() {
  return (
    <div >
      <ui5-shellbar
	
	primary-title="Corporate Portal"
	secondary-title="secondary title"
	
>
</ui5-shellbar>
<div className= "row">
<div className = "column" style= {{textAlign:"center", width:"30%"}}>
<ui5-list id="myList" class="full-width">
	<ui5-li icon="sap-icon://nutrition-activity" description="Tropical plant with an edible fruit" info="In-stock" info-state="Success">Pineapple</ui5-li>
	<ui5-li icon="sap-icon://nutrition-activity" description="Occurs between red and yellow" info="Expires" info-state="Warning">Orange</ui5-li>
	<ui5-li icon="sap-icon://nutrition-activity" description="The yellow lengthy fruit" info="Re-stock" info-state="Error">Banana</ui5-li>
	<ui5-li icon="sap-icon://nutrition-activity" description="The tropical stone fruit" info="Re-stock" info-state="Error">Mango</ui5-li>
</ui5-list>
     </div>
     <div className= "column" style = {{width:"30%"}}> 
       <Input></Input>
       <Input></Input>
       </div>
    </div>
    </div>
  );
}

export default App;
