﻿/*
Cerinta: Sa se gaseasca numele produsului a carui descriere este = N'أداء فائق في نقل السرعات.'; 
Hints: already given :p
*/

select p.name as Evrika
from SalesLT.Product p
join SalesLT.ProductModel pm
	on p.ProductModelID = pm.ProductModelID
join SalesLT.ProductModelProductDescription pmpd
	on pm.ProductModelID = pmpd.ProductModelID 
join SalesLT.ProductDescription pd
	on pmpd.ProductDescriptionID = pd.ProductDescriptionID
where pd.Description = N'أداء فائق في نقل السرعات.'; 
