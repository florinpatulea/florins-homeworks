-- set up tables

create schema edi;

create table edi.books (
	id INT PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	insertedAt DATETIME NULL DEFAULT GETDATE(),
	lastUpdatedAt DATETIME NULL CHECK(lastUpdatedAt >= GETDATE())
)

create table edi.descriptions (
	id INT IDENTITY (100, 1) PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	bookId INT NULL FOREIGN KEY REFERENCES edi.books(id)
)

create table edi.oldBooks (
	id INT PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	insertedAt DATETIME NULL DEFAULT GETDATE(),
	lastUpdatedAt DATETIME NULL CHECK(lastUpdatedAt >= GETDATE())
)

select * 
from edi.books

INSERT INTO edi.oldbooks (id, name, insertedAt, lastUpdatedAt)
VALUES (10, 'White and red old book', GETDATE(), GETDATE());

------------------------------------------------------------------------------------------------------------------------------------------
-- INSERT: 1 (valorile sunt hardcodate)

INSERT INTO edi.books (id, name, insertedAt, lastUpdatedAt)
VALUES (2, 'My first book', GETDATE(), GETDATE())

-- keyword-ul INTO este optional
INSERT edi.books (id, name, insertedAt, lastUpdatedAt)
VALUES (6, '6 book', GETDATE(), GETDATE()),
		(7, '7 book', GETDATE(), GETDATE()),
		(8, '8 book', GETDATE(), GETDATE())

-- coloanele sunt optionale
INSERT edi.books
VALUES (4, 'The computer book', GETDATE(), GETDATE())

-- dar doar daca valorile sunt exact ca si numar cat sunt coloanele 
INSERT edi.books
VALUES (5, 'The student book', GETDATE())

--> Msg 213, Level 16, State 1, Line 32 Column name or number of supplied values does not match table definition.

-- INSERT: 2 (valorile vin dintr-un select)

INSERT INTO edi.books (id, name, insertedAt, lastUpdatedAt)
SELECT 4, 'The carpenter book', GETDATE(), GETDATE()

-- sau dintr-o alta tabela
INSERT INTO edi.books (id, name, insertedAt, lastUpdatedAt)
SELECT id, name, insertedAt, GETDATE()
FROM edi.oldbooks;


-- INSERT 3: (into new table)

SELECT *
INTO edi.booksHist20190723
FROM edi.books
where id < 7;


-- error: tabela exista deja
SELECT * 
INTO edi.oldbooks
FROM edi.books

SELECT * FROM edi.booksHist20190723
-- drop table edi.booksHist20190722
----------------------------------------------

-- INSERT 4: (values from a stored procedure call):

CREATE PROCEDURE edi.TestInsertSP
AS
	SELECT 5, 'Via SP book', GETDATE(), GETDATE()
GO

CREATE PROCEDURE edi.TestInsertSPTemp1
AS
	SELECT *
	INTO #tempTbl
	from edi.books

	SELECT * 
	FROM #tempTbl
GO

EXEC edi.TestInsertSPTemp1 
-- tabela temporara creata on the fly la SELECT * INTO se sterge automat dupa call-ul 
-- de SP (si poate fi recreata la urm call / SELECT * INTO)


INSERT INTO edi.books (id, name, insertedAt, lastUpdatedAt)
EXEC edi.TestInsertSP

SELECT * FROM edi.books
SELECT * FROM edi.oldbooks

-- cateva insert-uri si in tabela descriptions

INSERT INTO edi.descriptions  (name, bookId)
VALUES ('Poti face join intre doua carti diferite dfsdfsdfsdfs', 1)

INSERT INTO edi.descriptions  (name, bookId)
VALUES ('Poti face join cu cartea insasi updated ....', 1)

select *
from edi.descriptions

INSERT INTO edi.descriptions  (id, name, bookId)
VALUES (3, 'Sunt mai multe tipuri de carti', 1)

-- Msg 544, Level 16, State 1, Line 93 Cannot insert explicit value for identity column in table 'descriptions' when IDENTITY_INSERT is set to OFF.

-- Fix: SET IDENTITY_INSERT [ database_name . [ schema_name ] . ] table { ON | OFF }

SET IDENTITY_INSERT edi.descriptions OFF

INSERT INTO edi.descriptions  (id, name, bookId)
VALUES (40, 'Sunt mai multe tipuri de join asdasdas', 2)

SET IDENTITY_INSERT edi.descriptions OFF

SELECT *
FROM edi.descriptions

-- FK-ul ne-hardcodat

SELECT top 1 id FROM edi.books where name = 'The student book'

INSERT INTO edi.descriptions  (name, bookId)
VALUES ('Delete description here.... ', ( SELECT id FROM edi.books where name = 'The student book' ))

SELECT *
FROM edi.descriptions
--------------------------------------------------------------------------------------------

-- UPDATE
UPDATE edi.descriptions 
	SET name = name + '.'

UPDATE edi.descriptions 
	SET name = name + ' test '
WHERE bookId = 4;

UPDATE d
	SET name = name + ';'
FROM edi.descriptions d
WHERE d.bookId = 2;

UPDATE old
	SET old.name = new.name,
		old.insertedAt = new.insertedAt,
		old.lastUpdatedAt = GETDATE()
FROM edi.oldbooks old
INNER JOIN edi.books new
ON old.id = new.id;

select *
from edi.oldbooks
-- IMPLICIT CONVERSION ? - crapa?
UPDATE d
	SET name = name + 11
FROM edi.descriptions d
WHERE d.bookId <7




-- FIX (function pt. explicit conversion)
UPDATE d
	SET name = name + CAST(11 AS VARCHAR)
FROM edi.descriptions d
WHERE d.bookId <7

-- IMPLICIT CONVERSION ? works

UPDATE s
	SET s.lastUpdatedAt = '2019-07-15'
FROM edi.books s

-- BETTER
UPDATE s
	SET s.name = 'Last updated at: ' + convert(varchar, s.lastUpdatedAt, 0)
FROM edi.books s

select *
from edi.books s

select getdate();


--------------------------------------------------------------------------------------------

-- DELETE


select *
from edi.descriptions

select *
from edi.books;

DELETE 
	FROM edi.books
WHERE id = 1

delete 
from edi.descriptions
where bookId = 1

--
DELETE 
	edi.booksHist20190722
WHERE id = 0

--
DELETE 
	FROM edi.booksHist20190722

--
DELETE edi.booksHist20190722

-- Filtrare prin JOIN
DELETE hist
	FROM edi.booksHist20190722 hist
JOIN edi.books s
ON hist.id = s.id;

select *
from edi.booksHist20190723

truncate
table edi.booksHist20190723
-- use with care :)

------------------------------------------------------------------------------------------
-- NULLability tests

select e.bookId, cast(e.bookId as varchar) + ' This is a test' te, e.bookId + 11 as nu
from edi.descriptions e
where bookId is NULL;


--------------------------------------------------------------------------------------------

-- set up date de test

CREATE TABLE edi.descriptionsTestDelete (
	id INT IDENTITY (10, 1) PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	bookId INT NULL FOREIGN KEY REFERENCES edi.books(id)
)

CREATE TABLE edi.descriptionsTestTruncate (
	id INT IDENTITY (10, 1) PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	bookId INT NULL FOREIGN KEY REFERENCES edi.books(id)
)

SET NOCOUNT ON;

DECLARE @i INT = 0
DECLARE @n INT = 1000000

WHILE @i < @n
BEGIN
	INSERT INTO edi.descriptionsTestDelete  (name)
	VALUES ('Dummy desc no #' + CAST(@i AS VARCHAR))

	INSERT INTO edi.descriptionsTestTruncate  (name)
	VALUES ('Dummy desc no #' + CAST(@i AS VARCHAR))
	
	SET @i += 1
END


SELECT COUNT(*) FROM edi.descriptionsTestDelete
SELECT COUNT(*) FROM edi.descriptionsTestTruncate

delete
FROM edi.descriptionsTestDelete

truncate 
table edi.descriptionsTestTruncate

SELECT *
FROM edi.books s
LEFT JOIN edi.descriptions d
ON s.id = d.bookId;

SELECT * 
FROM descriptions


-- TCL

