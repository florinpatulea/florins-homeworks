// Used in loading our data.
const fs = require('fs');
const csv = require('csv-parser');

// Used for ML logic.
const tf = require('@tensorflow/tfjs-node');

// Used to plot our data into a visually digestible fashion.
const plotly = require('plotly')('<user_name>', '<api_key')

// Constants used within the code.
const monthsInEachYear = 12;
const learningRate = 0.1;
const epochCount = 1000;

// Load the CSV data from disk into plain JavaScript arrays.
function loadData(filename) {
    let years = [];
    let months = [];
    let temps = [];

    return new Promise((resolve, reject) => {
        fs.createReadStream(filename)
            .pipe(csv())
            .on('data', (row) => {
                let year = parseInt(row.Year);
                years.push(year);

                let month = parseInt(row.Month);
                months.push(month);

                let temp = parseFloat(row.TempC);
                temps.push(temp);
            })
            .on('end', () => {
                console.log('CSV loaded successfully!');
                resolve([years, months, temps]);
            })
            .on('error', reject)
    })
}

// Use the plot.ly API to draw the graph into the cloud and provide a link to access it (for questions check links in readme).
function plotGraph(pointsX, pointsY, lineX, lineY) {
    var plotlyData = [
        {
            x: pointsX,
            y: pointsY,
            mode: 'markers',
            type: 'scatter',
            name: 'Average water temp reading',
        },
        {
            x: lineX,
            y: lineY,
            mode: 'lines',
            type: 'scatter',
            name: 'Our prediction model',
        }
    ];

    var plotlyLayout = {
        fileopt: 'overwrite',
        filename: 'mlTest',
        title: 'Data Labels Hover',
        layout: {
            showlegend: true,
            xaxis: {
                range: [1969, 2020], // [1970, 1987] -> [1969, 2020]
                title: 'Date'
            },
            yaxis: {
                range: [0, 15],
                title: 'Water temperature'
            }
        }
    };

    return new Promise((resolve, reject) => {
        plotly.plot(plotlyData, plotlyLayout, function (err, msg) {
            if (err) return reject(err);
            return resolve(msg.url);
        });
    });
}

// Get the norm of a tensor.
function normalizer(val) {
    return tf.tidy(() => {
        let min = val.min();
        let max = val.max();

        return val.sub(min).div(max.sub(min));
    });
}

// Remap a norm tensor to an other interval.
function remapNormalizedTensor(val, min, max) {
    return tf.tidy(() => {
        return val.mul(max - min).add(min);
    });
}

// Predict values using the line function.
function predictLinearRegression(x, a, b) { // ax + b = 0
    return tf.tidy(() => {
        return a.mul(x)
            .add(b);
    });
}

// Compute the MSE using predicted values and ground truths.
function lossMeanSquareError(prediction, expected) { // sum (O - o)^2
    return prediction.sub(expected).square().mean();
}

async function main() {
    let [y, m, t] = await loadData('./data/dataset.csv');

    let yTens = tf.tensor2d(y, [y.length, 1]);
    let mTens = tf.tensor2d(m, [m.length, 1]);

    let date = tf.tidy(() => { return yTens.mul(monthsInEachYear).add(mTens); });
    let temp = tf.tensor2d(t, [t.length, 1]);

    let normalizedDate = normalizer(date);
    let normalizedTemp = normalizer(temp);

    let a = tf.variable(tf.randomNormal([1], undefined, undefined, 'float32', 17054527));
    let b = tf.variable(tf.randomNormal([1], undefined, undefined, 'float32', 60707592));

    let optimizer = tf.train.sgd(learningRate);

    for (let iter = 0; iter < epochCount; iter++) {
        optimizer.minimize(() => {
            const prediction = predictLinearRegression(normalizedDate, a, b);
            const mse = lossMeanSquareError(prediction, normalizedTemp);
            console.log('Training - Step:\t' + iter.toString().padStart(epochCount.toString().length, ' ') + '/' + epochCount + '\tError:\t' + mse.dataSync()[0]);
            return mse;
        });
    }

    let linearRegressionAxisX = tf.range(0, 3.130, 0.005); // 1 -> 3.125
    let linearRegressionAxisy = predictLinearRegression(linearRegressionAxisX, a, b);

    let xValues = Array.from(remapNormalizedTensor(linearRegressionAxisX, 1970, 1986).dataSync());
    let yValues = Array.from(remapNormalizedTensor(linearRegressionAxisy, 1.86, 9.11).dataSync());

    let dateDisplayArray = Array.from(remapNormalizedTensor(normalizedDate, 1970, 1986).dataSync());
    let tempDisplayArray = Array.from(remapNormalizedTensor(normalizedTemp, 1.86, 9.11).dataSync());

    console.log('Done! Result: ' + await plotGraph(dateDisplayArray, tempDisplayArray, xValues, yValues));
}

main();

