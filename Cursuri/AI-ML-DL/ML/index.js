const plotly = require('plotly')('Shmiggy','FogHZR7gZcln6CAsHjsl');
const fs = require("fs");
const csv = require("csv-parser");
const tf= require('@tensorflow/tfjs-node');
const monthsPerYear = 12;

 function plotGraph(x, y){
    let data = [
        {
            x: x,
            y: y,
            type: 'scatter',
            mode: 'markers'

    }
];

    let options = {
        filename: 'mlCourse2',
        fileopt: 'overwrite'
    }

    return new Promise((resolve,reject) => {
        plotly.plot(data, options, (err,msg) => {
            if (err) return reject(err);
            return resolve(msg.url) ;
         });
    });
}

 function loadData(fileName) {
  let years = [];
  let months = [];
  let temps = [];

  return new Promise((reolve, reject) => {
    fs.createReadStream(fileName)
      .pipe(csv())
      .on("data", row => {
        let year = parseInt(row.Year);
        years.push(year);
        let month = parseInt(row.Month);
        months.push(month);
        let temp = parseFloat(row.TempC);
        temps.push(temp);
      })
      .on("end", () => {
        reolve([years, months,temps]);
      })
      .on("error", () => {
        reject();
      });
  });
}

async function main() {
    let [y, m, t] = await loadData("./data/dataset.csv");

    let yTens = tf.tensor2d(y, [y.length,1]);
    let mTens = tf.tensor2d(m, [m.length,1]);

    let date = tf.tidy(() => {
        return yTens.mul(monthsPerYear).add(mTens);
    })

     date.print();
    console.log('DOne! status:'+ await plotGraph(y,t));

  
}

main();
