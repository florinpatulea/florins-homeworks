//
//  Card.swift
//  WarGame
//
//  Created by Balan,Bogdan on 7/28/19.
//  Copyright © 2019 Balan,Bogdan. All rights reserved.
//

import Foundation

class Card {
    let suit: Suit
    let rank: Rank
    
    init(_ suit: Suit, _ rank: Rank) {
        self.rank = rank
        self.suit = suit
    }
}

enum Suit:String, CaseIterable {
    case Diamont = "D"
    case Heart="H"
    case Club="C"
    case Spade="S"
}

public enum Rank:String, CaseIterable {
    case Two = "2", Three = "3", Four = "4", Five = "5", Six = "6", Seven = "7", Eight = "8", Nine = "9", Ten = "10", Jack = "J", Queen = "Q", King = "K", Ace = "A"
    
    var value: Int {
        switch self {
        case .Ace: return 14
        case .Eight: return 8
        case .Five: return 5
        case .Four: return 4
        case .Jack: return 11
        case .King: return 13
        case .Nine: return 9
        case .Queen: return 12
        case .Seven: return 7
        case .Six: return 6
        case .Ten: return 10
        case .Three: return 3
        case .Two: return 2
        }
    }
    
}
