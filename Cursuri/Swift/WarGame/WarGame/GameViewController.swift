//
//  GameViewController.swift
//  WarGame
//
//  Created by Balan,Bogdan on 7/26/19.
//  Copyright © 2019 Balan,Bogdan. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {

    var cardTheme : CardTheme!
    
    @IBOutlet weak var firstPlayerDeck: UIImageView!
    @IBOutlet weak var secoundPlayerDeck: UIImageView!
    
    @IBOutlet weak var secoundPlayerCard: UIImageView!
    @IBOutlet weak var firstPlayerCard: UIImageView!
    @IBOutlet weak var firstPlayerNrCardLabel: UILabel!
    @IBOutlet weak var secondPlayerNrCardPlayer: UILabel!
    @IBOutlet weak var winnerLabel: UILabel!
    
    
    private var game: Game = Game(numberOfPlayers: 2)
    private var player1Card: Card?
    private var player2Card: Card?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        firstPlayerDeck.image = UIImage(named: cardTheme.rawValue)
        secoundPlayerDeck.image = UIImage(named: cardTheme.rawValue)
        updateCardsLabels()
    }
    @IBAction func firstPlayerDeckTappAction(_ sender: UITapGestureRecognizer) {
        firstPlayerCard.isHidden = false
        player1Card = game.getFirstCard(fromPlayer: 1)
        if let card = player1Card {
        firstPlayerCard.image = UIImage(named: card.rank.rawValue+card.suit.rawValue )
        }
        
        secoundPlayerCard.isHidden = false
        player2Card = game.getFirstCard(fromPlayer: 2)
        if let card = player2Card {
            secoundPlayerCard.image = UIImage(named: card.rank.rawValue+card.suit.rawValue )
        }
        updateGame()
    }
    
    private func updateGame() {
        let winnerIndex = game.compareCards()
        winnerLabel.text = "Player \(winnerIndex + 1) won"
        updateCardsLabels()
    }
    
    private func updateCardsLabels() {
        firstPlayerNrCardLabel.text = "Player 1 Nr. of cards : \(game.playersdecks[0].count)"
        secondPlayerNrCardPlayer.text = "Player 2 Nr. of cards : \(game.playersdecks[1].count)"
    }
}
