//
//  CardTheme.swift
//  WarGame
//
//  Created by Balan,Bogdan on 7/26/19.
//  Copyright © 2019 Balan,Bogdan. All rights reserved.
//

import Foundation

enum CardTheme: String, CaseIterable{
    case red = "Red_back"
    case yellow = "Yellow_back"
    case purple = "purple_back"
    case green = "Green_back"
    case gray = "Gray_back"
    case blue = "blue_back"
    
    
    
    var title : String {
        switch self {
        case .blue: return "Blue"
        case .gray: return "Gray"
        case .green: return "Green"
        case .purple: return "Purple"
        case .red: return "Red"
        case .yellow: return "Yellow"
        }
    }
}
