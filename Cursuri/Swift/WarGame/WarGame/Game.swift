//
//  Game.swift
//  WarGame
//
//  Created by Balan,Bogdan on 7/28/19.
//  Copyright © 2019 Balan,Bogdan. All rights reserved.
//

import Foundation

class Game {
    private var numberOfPlayers: Int
    private var allCards = [Card]()
    private(set) var firstPlayerDeck = [Card]()
    private(set) var secoundPlayerDeck = [Card]()
    private(set) var playersdecks = [[Card]]()
    
    init(numberOfPlayers : Int) {
        self.numberOfPlayers = numberOfPlayers
           createCardDeck()
    }
    
    private func createCardDeck() {
        generateCardDeck()
        dealCards()
    }
    
    func generateCardDeck() {
        for suit in Suit.allCases {
            for rank in Rank.allCases {
                allCards.append(Card(suit, rank))
            }
        }
        allCards.shuffle()
    }
    
    func dealCards() {
        let numberOfCardsPerPlayer = allCards.count / numberOfPlayers
        for player in 0..<numberOfPlayers {
            let cardsForPlayer = allCards[player * numberOfCardsPerPlayer ..< (player + 1) * numberOfCardsPerPlayer]
            playersdecks.append(Array(cardsForPlayer))
        }
    }
    
    func getFirstCard(fromPlayer playerIndex : Int ) -> Card? {
        return playersdecks[playerIndex - 1].first
    }
    
    func compareCards() -> Int {
        let valuesOfAllFirstCards = playersdecks.map {$0.first!.rank.value}
        let maxCard = valuesOfAllFirstCards.max()
        let indexOfMaxCard = valuesOfAllFirstCards.indices.filter{valuesOfAllFirstCards[$0] == maxCard}
        return giveCardsToRandomPlayer(from: indexOfMaxCard)
    }
    
    private func giveCardsToRandomPlayer(from seqance : [Int]) -> Int {
        let indexOfWinner = Array(seqance).shuffled().first
        playersdecks[indexOfWinner!].append(contentsOf: getCardsToTake())
        return indexOfWinner!
    }
    
    private func getCardsToTake() -> [Card] {
        var cardsToTake = [Card]()
        for player in 0..<numberOfPlayers {
            cardsToTake.append(playersdecks[player].removeFirst())
        }
        return cardsToTake
    }
}
