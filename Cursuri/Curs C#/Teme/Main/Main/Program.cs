﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{

    class Program
    {


        static void Main(string[] args)
        {
            List<Persoana> personList = new List<Persoana>();

            Student s1 = new Student
            {
                nume = "Patulea",
                prenume = "Florin",
                nrMatricol = "2232",
                medie = 9.52,
                adresa = "Bv"


            };
            Student s2 = new Student
            {
                nume = "Mihai",
                prenume = "Andrei",
                nrMatricol = "1234",
                medie = 6.22,
                adresa = "Bv"


            };
            Persoana p1 = new Persoana
            {
                nume = "Gigel",
                prenume = "Cristi",
                adresa = "Ct"
            };
            Persoana p2 = new Persoana
            {
                nume = "Vlad",
                prenume = "Gabriel",
                adresa = "Vl"
            };

            personList.Add(s1);
            personList.Add(s2);
            personList.Add(p1);
            personList.Add(p2);

            Console.WriteLine(p1.nume);

            Console.WriteLine("Sortare dupa nume");
            personList.Sort((x, y) => x.nume.CompareTo(y.nume));
            
            foreach (Persoana o in personList)
            {
                Console.WriteLine(o+ $":{o.nume}");
            }
            

            Console.WriteLine("Sortare dupa prenume");


             personList.Sort((x, y) => x.prenume.CompareTo(y.prenume));
            foreach (Persoana o in personList)
            {
                Console.WriteLine(o + $":{o.prenume}");
            }





        }
    }
}
