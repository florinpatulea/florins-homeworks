﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animal
{
    public class AnimalController
    {
        readonly IAnimalRepository _animalRepository;

        public AnimalController(IAnimalRepository animalRepository)
        {
            _animalRepository = animalRepository;
        }

     
        

        public void Insert(Animals a)
        {
            _animalRepository.Add(a);
        }

      public Animals GetById(int id)
        {
            return _animalRepository.GetById(id);
        }

        public IEnumerable<Animals> GetAll() => _animalRepository.GetAll();

        
    }
}
