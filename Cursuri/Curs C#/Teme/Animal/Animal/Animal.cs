﻿using System;

namespace Animal
{
    public class Animals
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }



        public override string ToString() => $"Person info : {Id} {Name} {Description}";
        
    }
}
