﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Animal
{
    public interface IAnimalRepository
    {

        void Add(Animals a);
        void Delete(Animals a);
        IEnumerable<Animals> GetAll();
         Animals GetById(int id);

    }
    public class AnimalRepository
    {
        readonly List<Animals> _animal = new List<Animals>();

        public void Add(Animals a)
        {
            _animal.Add(a);
        }

        public void Delete(Animals a)
        {
            _animal.Remove(a);
        }

       public Animals GetById (int id)
        {
            foreach (var p in _animal)
            {
                if (p.Id == id)
                {
                    return p;
                }
            }
            return null;
        }

        public IEnumerable<Animals> GetAll() => _animal;

       
    }
}
