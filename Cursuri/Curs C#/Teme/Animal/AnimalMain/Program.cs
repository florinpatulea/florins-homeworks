﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animal;

namespace AnimalMain
{
    class Programs
    {
        static void Main(string[] args)
        {
            Animals a1 = new Animals()
            {
                Name = "Tiger",
                Description = "Red",
                Id = 1
                
            };


            Animals a2 = new Animals()
            {
                Name = "Elephant",
                Description = "Green",
                Id = 2

            };

            AnimalRepository repo = new AnimalRepository();

            repo.Add(a1);
            repo.Add(a2);
            

            IEnumerable<Animals> animale = repo.GetAll();

           

            foreach (Animals a in animale)
            {
                Console.WriteLine(a);
            }

        }
    }
}
