﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace Animal.Test
{
    [TestClass]
    public class AnimalControllerTest
    {
    [TestMethod]
    public void GetData_Test()
        {
            Mock<IAnimalRepository> repMock = new Mock<IAnimalRepository>();

            Animals a1 = new Animals()
            {
                Name = "Tiger",
                Description = "Red",
                Id = 1

            };

            repMock.Setup(x => x.GetById(1)).Returns(a1);

            AnimalController controller = new AnimalController(repMock.Object);

            Animals result = controller.GetById(1);

            Assert.AreEqual(a1.Name, result.Name);
            Assert.AreEqual(a1.Description, result.Description);
            Assert.AreEqual(a1.Id, result.Id);

        }

        [TestMethod]
        public void GetAllData_Test()
        {
            Mock<IAnimalRepository> mock = new Mock<IAnimalRepository>();
            Animals a1 = new Animals()
            {
                Name = "Tiger",
                Description = "Red",
                Id = 1

            };


            Animals a2 = new Animals()
            {
                Name = "Elephant",
                Description = "Green",
                Id = 2

            };

            List<Animals> list = new List<Animals>
            {
                a1,
                a2
            };

            mock.Setup(x => x.GetAll()).Returns(list);

            AnimalController controller = new AnimalController(mock.Object);

            List<Animals> result = controller.GetAll().ToList();

            Assert.AreEqual(2, result.Count);



        }
    }
}
