﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoCurs4.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DemoCurs4
{
	public class Startup
	{
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc();

			services.AddTransient<IBuildingRepository,EFBuildingRepository>();
            //interactionare cu sql 
            services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(_configuration["ConnectionStrings:DefaultConnection"]));

        }

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			//app.UseMvc(ConfigureRoutes);

			app.UseDeveloperExceptionPage();
			app.UseStatusCodePages();

			app.UseMvcWithDefaultRoute();

			//app.Run(async (context) =>
			//{
			//	await context.Response.WriteAsync("Hello World!");
			//});
		}

		//private void ConfigureRoutes(IRouteBuilder routes)
		//{
		//	routes.MapRoute(null, "{controller}/{action}/{id?}");
		//}
	}
}
