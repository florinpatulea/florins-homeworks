﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoCurs4.Models
{
    public class EFBuildingRepository : IBuildingRepository
    {
        private readonly ApplicationDbContext _context;
        public EFBuildingRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Building> Buildings => _context.Buildings;
    }
}
