﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoCurs4.Models;
using Microsoft.AspNetCore.Mvc;

namespace DemoCurs4.Controllers
{
	public class BuildingController : Controller
	{
		readonly IBuildingRepository _repo;

		public BuildingController(IBuildingRepository repo)
		{
			_repo = repo;
		}

		public IActionResult Index()
		{
			var buildings = _repo.Buildings;
			return View(buildings);

			//return View("MyCustomView",)
		}
	}
}