﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoCurs4.Models
{
	public class BuidingRepository
	{
		readonly List<Building> _buildings = new List<Building>
		{
			new Building
			{
				Id=1,
				Name="Cerner_1",
				Address="Turnului 5",
				Height=100,
				Width=100
			},
				new Building
			{
				Id=2,
				Name="Cerner_2",
				Address="Turnului 5_2",
				Height=200,
				Width=200
			},
		};

		public List<Building> Buildings => _buildings;
	}
}
