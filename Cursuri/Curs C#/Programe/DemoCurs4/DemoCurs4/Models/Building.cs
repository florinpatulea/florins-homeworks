﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoCurs4.Models
{
	public class Building
	{
		public int Id { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
	}
}
 