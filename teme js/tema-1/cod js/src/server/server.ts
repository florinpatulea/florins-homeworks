import express from "express";
import bodyParser from "body-parser";

export default class Server {
    private port:number;
    private app: Express.Application;

    public constructor(app:express.Application, port:number){
        this.port=port;
        this.app=app;
    }

    private configApp(){
            this.app.use(bodyParser.json());
            this.app.use(bodyParser.)

    }
}