"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express")); // express - creeaza server
const body_parser_1 = __importDefault(require("body-parser"));
const mockedData = __importStar(require("../resources"));
const app = express_1.default();
const port = 8080;
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: false }));
app.get("/", (request, response) => {
    response.send("Hello world");
});
app.get("/lists", (req, res) => {
    res.send(mockedData.list);
});
app.get("/list/:id", (req, res) => {
    let idFromRequest = req.params.id;
    let requestList = mockedData.lists.filter(el => el.id === idFromRequest);
    res.send(requestList);
});
app.post("/toConsole", (req, res) => {
    console.log(req.body.myMessage);
});
app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}!`);
});
//# sourceMappingURL=index.js.map