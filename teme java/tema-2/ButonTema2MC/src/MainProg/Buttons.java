package MainProg;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Buttons {
	
	JFrame f = new JFrame("Aplicatie");
	JButton b1 = new JButton("Buton 1");
	JButton b2 = new JButton("Buton 2");
	
	JButton ultimaApasare = null;
	boolean apasare_1 = false;
	boolean apasare_2 = false;
	
	public void initializareFrame() {
		f.setSize(400, 400);
		f.setLayout(null);
		f.setVisible(true);
		f.add(b1);
		f.add(b2);
	}
	
	public void initializareButoane() {
		
		
		b1.setBackground(Color.gray);
		b1.setBounds(70, 100, 90, 90);
	
		b2.setBackground(Color.gray);
		b2.setBounds(250, 100, 90, 90);
		
		
		
	}
	
	public void Schimbare() {
		
		Timer aprindere = new Timer(500, new ActionListener() {
			
			private int Nr = 0;
			private int maxNr = 100;
			private boolean pornit = false;
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if ( ultimaApasare != null)
				{
					if (Nr >= maxNr) {
					//	ultimaApasare.setBackground(Color.gray);
						ultimaApasare.setText("NoTIME");
						
						
					} else {
						pornit = !pornit;
						Nr++;
					}
				}
				if (apasare_1 == true || apasare_2 == true)
				{
					ultimaApasare.setBackground(pornit ? Color.blue : Color.gray);
					ultimaApasare.setText("Apasat");
					ultimaApasare.setForeground(Color.red);
				}
				if (apasare_1 == true && apasare_2 == true)
				{
					ultimaApasare.setBackground(pornit ? Color.blue : Color.yellow);
					b1.setBackground(pornit ? Color.blue : Color.yellow);
					b2.setBackground(pornit ? Color.blue : Color.yellow);
				}
				
			}
		});
		
		aprindere.start();
		
		b1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				ultimaApasare = b1;
				apasare_1 = true;
			}
		});
		
		b2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				ultimaApasare = b2;
				apasare_2 = true;
				
			}
		});
	}
			

}
