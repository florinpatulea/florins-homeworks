package ButonTema2;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.*; 
import javax.swing.*;


public class Main {
	
	private JFrame f;

public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				
					Main window = new Main();
					window.f.setVisible(true);
				
				
			}
		});
		
		
	}
	
	public Main() {
		init();
	}
	

	JButton lastPressed = null;
	boolean pressed_1 = false;
	boolean pressed_2 = false;
	
	public void init()
	{
		f = new JFrame("Aplicatie");
		f.setSize(700, 700);
		f.setLayout(null);
		

		JButton b1= new JButton("Buton 1");
		b1.setBackground(Color.gray);
		
		b1.setBounds(200,200,95,95);
		f.getContentPane().add(b1);
		
		JButton b2 = new JButton("Buton 2");
		b2.setBackground(Color.gray);
		
		b2.setBounds(400, 200, 95, 95);
		f.getContentPane().add(b2);
		
		
		Timer blink = new Timer(500, new ActionListener()
				{
			private int count = 0;
			private int maxCount = 20;
			private boolean on = false;

					@Override
					public void actionPerformed(ActionEvent e) {
						if (lastPressed!=null)
						{
							if (count >= maxCount) {
								lastPressed.setBackground(Color.gray);
								((Timer)e.getSource()).stop();
								
							}else {
								lastPressed.setBackground(on ? Color.blue:Color.gray);
								on = !on;
								count++;
							}
						}
						if (pressed_1 == true || pressed_2 == true)
						{
							lastPressed.setBackground(on ? Color.blue : Color.gray);
							
						}
						
						if (pressed_1 == true && pressed_2 == true)
						{
							lastPressed.setBackground(on ? Color.blue:Color.yellow);
							b1.setBackground(on ? Color.blue : Color.yellow);
						}
						
						
						
					}
			
				});
		
		blink.start();
		
		b1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				lastPressed = b1;
				pressed_1 = true;
				
			}
		});
		
		b2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				lastPressed = b2;
				pressed_2 = true;
				
			}
		});
	}
	
	
	

}
